# 9. Embedded programming

The main goal of this task is to learn the basics of embedded programming. In general, we programmed two diferent MCUs: one belonging to the ATmega family (the Olimex and Arduino UNO have this type of microcontroller), and the other one included in the ATtiny 412. The idea is to create a simple program (blinking led) using the Olimex AVR-MT128 development board integrated with Arduino IDE and the ATtiny 412. The Eagle files as well as the **.rml** files for the milling machine can be found [here](../images/Images/PCB.zip).

## Steps
The following steps need to be carefully taken before running the sketch

*   In ARDUINO IDE From Tools>Boards select: ATmega128 
*   From Tools>Bootloader select: No Bootloader 
*   From Tools>Clock select: External 16 MHz 
*   From Tools>BOD select: BOD Disabled 
*   From Tools>Compiler LTO: select: LTO Disabled 
*   Connect JTAG and Olimex board and power the board 
*   From Tools>Port select: the COM “x”-port your JTAG is connected to
*   From Tools>Programmer: select: Olimex JTAG



## Code Description
The purpose of the following code is to blink a led at two different frequencies depending on the botton that is pressed. If the upper botton is
held then the LED will blink at a frequency of 5Hz and if it is pressed the left one then the LED will blink at a rate of 1hz. To stop the LED
the lower botton needs to be used.


```
// include the library code:
#include <LiquidCrystal.h>
// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 28, en = 30, d4 = 32, d5 = 33, d6 = 34, d7 = 35;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
int pinLed = 38;
int pinSwitch1 = 44;
int pinSwitch2 = 40;
int pinSwitch3 = 43;

void setup() {
//Pin29 is R/W pin for LCD
  pinMode(29, OUTPUT);
  digitalWrite(29, LOW);
  
  // Pin to light the led
  pinMode(pinLed,OUTPUT);
  digitalWrite(pinLed,LOW);
  pinMode(pinSwitch1,INPUT);
  pinMode(pinSwitch2,INPUT);
  pinMode(pinSwitch3,INPUT);
  
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.setCursor(4, 0); // top left
  lcd.print("Press upper ");
  lcd.setCursor(2, 1); // bottom left
  lcd.print("or left botton!");
  delay(1500);
}
void loop() {
 

  if ( digitalRead(pinSwitch1) == LOW) // If it is pressed then tunr the LED on
    {  
       while (digitalRead(pinSwitch2)==HIGH )
       {
       digitalWrite(pinLed, HIGH);
       delay(200);
       digitalWrite(pinLed, LOW);
       delay(200);
       
       }
    }
    else if (digitalRead(pinSwitch3)== LOW)
    {
       while (digitalRead(pinSwitch2)==HIGH )
       {
       digitalWrite(pinLed, HIGH);
       delay(1000);
       digitalWrite(pinLed, LOW);
       delay(1000);
       
       }
    }  
    
   else
    {
       digitalWrite(pinLed, LOW);
    }  
}

```
## Figures
Special care needs to be taken when programing the pins in the Ardouino IDE since they do not correspond with pin specifications in the 
Olimex board. For this project were used the pins corresponding to the bottons (PA0, PA1 and PA4), relay(PA5) and display(PC1). The corresponding 
difference in the pin distribution is shown in the folllowing figures.

![](../images/Images/MegaCorePins.jpeg)
![](../images/Images/Circuit.jpg)


## Blinking a LED at different frequencies using the Olimex AVR-MT128.

<iframe src="https://player.vimeo.com/video/401482457" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

## Useful links

(https://www.olimex.com/Products/AVR/Development/AVR-MT128/)

(https://www.electroschematics.com/simple-arduino-led-projects/)

## Using the ATtiny412 to flash a LED.

In this section, a simple board that contains the ATtiny 412 and surrounding components such as a LED, resistance,
switch and capacitor is developped. The chip should run a simple set of instructions in order to turn the LED on and off. One of the biggest 
challenges of using this processor is that it does not have bootloader. Thus, an Arduino UNO is used as the Unified Program and
Debug Interface (UPDI) programmer. Schematics and useful information is futher provided.

## Work description

In most of the tutorials, it is adviced to use an electrolitic capacitor of 10 micro farad to disable the reset that is triggered everytime 
a sketch is uploaded, and that would cause the code to overwrite in the flash of the Arduino instead of the ATtiny. In other words, 
it stops the programmer from end up in a bootloader. Notice that in this case, the diagram lacks this capacitor because for unknown reasons 
it stopped the sketch to be properly uploaded to the Arduino. In this case the pin 6 of the Arduino is configured as UPDI, and needs to be
connected to the reset pin of the ATtiny using a resistence as it is shown in the figure below.
![](../images/Images/Board_Connection.jpg)

A current limiting resistor is attached to the cathode to protect the LED from high current driven by the pin of the ATtiny. A higher value
of resistence would make the LED to light with less intensity. Moreover, a pushbutton is used and depending on its position the LED will be
turned on and off. Notice, that the LED is placed in the physical pin 3 and the pushbutton in the physical pin 2. This information is useful 
to understand the sketch.
![](../images/Images/Circuit_ATtiny.jpg)

The next picture illustrates the pin mapping. Futhermore, the pins used in the sketch are marked in red.
![](../images/Images/Pin_Distribution.jpg)

IN order to design the PCB, I used the software Eagle 9.6.2 which is a Tool for designing bluprints and layouts. This software generates two **.png** files containig schematic view of the PCB (TOP and OUTLINE). These two files were introduced in the link https://mods.cba.mti.edu to generate the **.rml** files, which are used in the milling machine. The layout obtained is shown below.

![](../images/Images/PCB_ATtiny.jpg)

The **.rml** files obtained from the previous steps are uploaded to the software used for the milling machine (VPanel for SRM-20) and proceed to cut the PCB. Images of the software and the machine are displayed below.

![](../images/Images/Milling4.jpg)
![](../images/Images/Milling1.jpg)
![](../images/Images/MIlling2.jpg)
![](../images/Images/Milling3.jpg)

After cutting the PCB the next step is to solder the components on the board, the following image shows the final result.

![](../images/Images/PCB_ATtin.jpeg)



## Transform an Arduino into a UPDI programmer

- Close all instances of the Arduino IDE to avoid errors.

- Download and extract the UPDI programmer sketch. It can be found in: https://github.com/SpenceKonde/jtag2updi.

- Open the jtag2updi folder after extracting the download.

- Open the sketch jtag2updi.ino and upload it to the Arduino board. The .ino file will appear empty and that is fine as all the code
is contained in the other files in the same folder.

## Programming the ATtiny
By default, the Arduino IDE does not support the ATtiny 412 from the Arduino IDE, therefore it is necessary to add support for it in the
Arduino Board Manager.

- From the Arduino IDE, open the Preferences option and scroll down to Additional Board Managers URLs.

- Copy and paste the following link: https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json.
If another URL has already been introduced in the board manager, just add a comma before pasting the new one.

- Press "OK" and restart the Arduino IDE.

- From Tools option in the Arduino IDE, install the ATtiny 412 in the Board Manager. After following the previous steps, the IDE should be set up as follow:

![](../images/Images/megaTinyCore_into_Arduino_IDE.jpg)

Notice, that since the ATtiny is being powered using 5v from the Arduino, one can use 20Mhzh as clock frequency. In case a lower voltage
is used, then the clock frequnecy needs to be reduced.


## Set of instructions

```
const int ledPin =  1;      // LED pin
const int switchPin = 0;     // Pushbutton pin
int switchPosition = 0;         // Variable that stores the position of the switch

void setup() {
  // Initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
  randomSeed(300);
}

void loop() {
  // Read the state of the pushbutton value:
  switchPosition = digitalRead(switchPin);

 
  if (switchPosition == LOW) {
    // turn LED on and off
    
    digitalWrite(ledPin, HIGH);
    delay(200);
    digitalWrite(ledPin, LOW);
    delay(200);
      
  // If the switch is open, turn the LED off  
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}
```


## Blinking a LED (Video)

<iframe src="https://player.vimeo.com/video/436952146" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


 
## Useful links

- https://www.electronics-lab.com/project/getting-started-with-the-new-attiny-chips-programming-the-microchips-0-series-and-1-series-attiny-with-the-arduino-ide/
- https://github.com/SpenceKonde/megaTinyCore/blob/master/MakeUPDIProgrammer.md
- http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf
- https://www.youtube.com/watch?v=AL9vK_xMt4E
