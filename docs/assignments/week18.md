# 3. Aplications and Implications

The task this week is to propose a possible final project that includes some of the knowledge adquired during the course. Essential ideas, and a list of possible tools and hardware to implement such a project needs to be provided.
Moreover, it was required to describe the implementation steps and if posible to report the overall cost of the final product.

# Project: Smart beekeeper using Arduino.

The fundamental goal of this project is to endow the apiculture activities with modern technologies such an Arduino platform and a group of sensors
and maybe actuators that allows the measuring of important variables and therefore obtain a more efficient production of honey, by correctly adjusting
them when neccesary.
# Related works 
A great amount of projects like this can be found all over Internet, but each of them with particular characteristics depending on the enviroment where the beehive is placed at and the available resources (hardware and electronic components). For example, [here](https://www.researchgate.net/publication/311953539_Honey_Bee_Colonies_Remote_Monitoring_System) a honey bee colonies remote monitoring system is presented. I also found [an electronic beehive](https://www.researchgate.net/publication/321137272_Design_of_an_automated_hive_for_bee_proliferation_and_crop_betterment), that recollects honey automatically, regulates the temperature of the inner hive and distributes food for bees. However, in this project I tried to integrate some of the techniques used in many of the already proposed works, to achieve more accurate results an reduce the cost of production. 

# Materials and components
-1xArduino UNO ( 8.77€ in https://www.banggood.com/)  

-1xEPS 8266 based Wifi shield (2.85€ Ali Express)  

-2x DHT22 humidity and temperature sensor ( 4.55€ in https://www.banggood.com/)  

-1xSwitched power supply (3.61€ in https://www.banggood.com/)

-HX711 Module + 20kg Aluminum Alloy Scale Weighing Sensor Load Cell Kit Geekcreit for Arduino(7.81€ in https://www.banggood.com/)  

-MOSFET IRF540N (3€ in https://www.banggood.com/).

-12v and 0.25A fan (2€ in https://www.aliexpress.com/).

Total estimated cost: 32.59 €

## Project description
Honey bees maintain the temperature of the brood nest between 32°C and optimally 35°C so that the brood develops normally.
When the temperature in the nest is too high the bees ventilate by fanning the hot air out of the nest or use evaporative cooling mechanisms.
When the temperature is too low bees generate metabolic heat by contracting and relaxing their flight muscles (having uncoupled the wings from them).
In general these efforts they make in order to keep this parameter in rage reduce
their productivity. So the task consist of sensing variables such as temperature and humidity in order to act upon their changes when it is needed
and therefore obtain higher levels of honey production. To achieve this, one sensor is placed outside (in the top) the beehive to capture the variations in the temperature
and humidity of the enviroment, and another one will be located inside the broom box in order to guarantee the correct atmosphere for the eggs.
Both sensors will be sending data to the Arduino every 10 minutes (alternatively, I could use a PCB with Attiny chips). Dependinng on the temperature values a cooling system is activated. This can be
made using a 12v and 0.25A fan connected to the Arduino with a MOSFET wich allows us to drive higher current which can not be done using only the Arduino, and 
also allows us to interface with higher values of voltage (12v in this case that can be provided externally with a switched power supply). A simple diagram
of these connections are provided below. In order to control the amount of honey that is being produced, a Scale Weighing Sensor (HX711 amplifier with scale) is placed at the bottom of the structure and it will also sends the measures to the Arduino. An useful Arduino library to interface with the HX711 can be found [here](https://github.com/bogde/HX711#readme).
All the values collected from the sensors will be sent over Wifi by means of a shield attached to the Arduino board and using HTTP protocol, to be stored in adafruit.io where different types of graphics can be 
obtained in order to have a better view of the parameters behavior. A self watering based on Arduino can even be implemented in order to keep the 
bees as much as possible inside the perimeter wihout pushing them to go around. We leave this last part out of the scope of this project because there are other less expensive and 
less time consuming ways to accomplish this without using an Arduino. 
Since the hive is considered to be located relatively close from home (in the backyard), a power cord can easily reach the place to provide energy for the 
Arduino through a switched power supply that can be either fabricated using and LM317 regulator or bougth for only 3.61€.
![](../images/Images/Circuit.jpeg)

In the following picture, (1) is the Arduino UNO; (2) is the EPS 8266 based Wifi shield; (3) is the switched power supply; (4) is the Analog-to-Digital Converter HX711, that connects to the weighing sensor. Even though in the picture it is not specified the interconnections between the boards, [here](https://create.arduino.cc/projecthub/pvalyk/beekeeping-with-arduino-4216bb) I provide a link where similar boards are used; and the connection diagram is shown. Notice that the yellow cable connects the HX711 Module in the external box with the Scale Weighing Sensor placed at the bottom of the hive; the blue cable links the Arduino with the cooling system (12v fan); and the red one is connected from the DHT22 external sensor to the Arduino.

![](../images/Images/Fablab.jpg)

# What processes will be used? 
Notice that trying to find a plastic box suitable to fit the Arduino, the switching power and other components might not be an easy task because it is quite unlikely to find one box with the exact dimensions. Thus, collecting these dimensions and performing 3D printing of the box is an important task. Also notice that this will protect the electronic components from the weather changes since this parts are supposed to be outside of the beehive itself and exposed to rain and sun. Another easy way to achieve this task without the use of 3D printers, which may not be available in all places, could be by using molding and casting techniques. To this end, a rigid frame called matrix (hollowed-out block where melted plastic will be poured into) probably made out of wood is important. 

# What will you design?
In case of using 3D printing technologies to build the box that will protect the boards from the weather, Autocad Fusion 360 is a good option to start developping the design. Additionally, Blender is an open source 3D creation suite that could also be used. I could even design my own regulated power supply using the regulator LM317 which costs only 0.86 US. The schematic can be created and later simulated in OrCAD. The process of getting this boards ready starts with a computed aided design (CAD), and after a few iterations we can get the needed files for creating the PCB using the Roland Milling Machine (SRM-20) and finally make the corresponding soldering. Notice that this implementation process can also be applied in case a PCB with Attiny chip is fabricated to replace the Arduino board. The boxes that compose the hive can be made out of wood; and to this end, I can use the laser cutter in FABLAB to fabricate the prototype before implementing the project in large scale.

# What questions need to be answered? 
- Is the project affordable?

- Are the electronic components easy to find in the market?

- Are there any other platforms better than adafruit.io to process and visualize IoT data?

- The project is mainly conceived to work close to a house or any other facility where 110/220 v source can be obtained to power the switched power supply and therefore the Arduino. But in case the project needs to be set up in remotes enviroments (far from the city in rural places), what could be the power source used to maintain the Arduino and the cooling system active for a reasonable period of time without needing to replace battery constantly and affecting the natural production system of the bees? 

- How to efficiently place the sensors, both outside and inside the beehive, and how distant they should be between each other to avoid spatial correlation in the measurements?

# How will it be evaluated?
In order to be sure that the sensors are performing correct measurements and the Arduino is receiving/sending the sensed variables correctly, I decided to implement first the project at small scale. This is, using a small breadboard where I can placed all the electronic components and make the proper connections to the Arduino. The idea is to intentionally change the variables (temperatures and humidity) and see how the design responds to those changes. This will allow me to test the design before putting all the pieces into the real enclosure. Fake weight can be increasingly put on top of a surface where the weighing sensor is placed in order to simulate the variabilities in the weight of the hive when the bees start producing honey. By forcing the physical variables, we can see the system response, which will ensure the minimum funcionality of the system. This first trial is very important to avoid mistakes once the real application is set up. Some components can be evaluated also separatedly before implementing the project, for example, the Arduino (or PCB), LM317, and the MOSFET;  thus, I can check if there is any malfunctioning on any of them. Another important thing is to ensure that the system works for a certain period of time without falling into unstable conditions. Check the operation temperature of each component specially when the Arduino board needs to trigger the cooling system because eventhough a MOSFET is used, high levels of temparatures and current drain can cause the Arduino to burn. 

## Conclusions
A very inexpensive project has been presented, proving that simple and economic solutions can be effetive to achieve a particular goal. In the future this project can be improved with some new and more innovative
ideas such as the use of a Rasperry Pi which allows for other benefits.

```


