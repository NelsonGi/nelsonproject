# 16. Interface and application programming  
# Project overview 

The main goal of this week task is to create graphical user intafaces (GUIs) using different platforms and compared them in terms of complexity in order to determine
advantage and disadvanteges between the different procedures. To this end, two different applications are implemented; firstly, the GUI shoud be able to send instructions to the Arduino, which activates a servo motor, thus, serial 
communication is an essential part of this task. Another application includes commanding the ATtiny 412 from the GUI in the computer, to flash a LED embedded in the board. I used Python and Processing to develope the GUIs. To have access to the group assigment this week, click [here](https://dechevar19.gitlab.io/gitlab-project/assignments/week17/).

## First task description ("Controlling a servo motor using Arduino UNO") 

For this application, the pin 2 of the Arduino was used as an output to exitate the servo motor with a pulse-width modulation (PWM). Notice that even though the Arduino UNO has dedicated hardware internally connected to certain pins (to know these pins specifications in different Arduino versions, click [here](https://www.arduino.cc/reference/en/language/functions/analog-io/analogwrite/)) to genrate PWM, which is nice, because it allows PWM to take place in the background while other code is being acted on; I decided to execute this function via software using pin 2 because this was the only application executed by the microcontroler. But if more complex applications are requested, then one would need to consider using any of the pins 3, 5, 6, 9, 10, 11. In general, the setup is described as follows: two main buttons are created in the user interface—two versions of the GUI were implemented, one using **Python (Tkinter)** and another one using **Processing**—that allows the user to either move the servo to one side or the other. When the user click on the open button, instructions are sent to the Arduino (two character deppending on the pressed button), which further generates a PWM signal to move the servo in the clockwise direction; if the user then click on the close button, the Arduino motor modulates the width of the pulse in the way that the servo moves the other way around. Notice that the communication between the Arduino and the computer is made via Serial protocol and even though binary data is usually more efficient (requiring fewer bytes to be sent), I decided that applications should send and receive human-readable text (characters in this case) though serial port because text messages are easier to implement and debug. Information about the serial protocol (data frame structure, synchronization, etc) and its functioning can be found [here](https://circuitdigest.com/tutorial/serial-communication-protocols#:~:text=Now%20if%20the%20data%20is,communication%20channel%20in%20Serial%20Communication.). Special care needs to be taken with the pin distribution
of the servo so we can match the outputs of the Arduino board to the correct entries of the servo(signal, GND and 5v). For this assigment it was only supposed to move the motor but in case something heavy is attached to it, an external power source needs to be used in oder to prevent the USB port to burn.

![](../images/Images/servo-motor-pinout.jpg)

## Code executed by Arduino
Here, the code used for Arduino is shown and commented. The file can be found [here](https://gitlab.com/NelsonGi/nelsonproject/-/blob/master/docs/images/Images/Arduino_servo.ino).


```
#include <Servo.h>    // Including the required library to carefully use internal timers for servo control
Servo servoMotor;     // Create a variable servoMotor of type Servo
int servPin = 2;      // Declaration of the pin to which the servo is attached
int pulseMin = 1000;  // The pulse width, in microseconds, corresponding to the minimum (0-degree) angle on the servo
int pulseMax = 2000;  // The pulse width, in microseconds, corresponding to the maximum (180-degree) angle on the servo

void setup() {       // Note that these lines will execute only once in the code
 servoMotor.attach(servPin,pulseMin,pulseMax); // Attach the Servo variable to the pin 2.
 Serial.begin(9600);                      // Sets the data rate in bits per second (baud) for serial data transmission.
 }

void loop() {      // Note that these lines will execute continiously once the application is triggered.

  if(Serial.available()){
    
    char val = Serial.read();  // Reads incoming serial data.
    if(val == 'u'){            // If the character type date is read from the serial port, execute an action
                               // notice that this character is sent when the user press a button in the user interface.
       servoMotor.write(180);   // if 'u' received move motor 180 degree
       delay(200); 
    }
    if(val == 'd'){
       servoMotor.write(0);  // if 'd' is received move motor 0 degree
    }
  }
}
```

## Code for GUI using Python 
A GUI is a desktop application that provides you with an interface that helps you interact with different type of hardwares and enriches your experience. In this section the code used to implement the GUI in Python (Tkinter) is presented. Python offers multiple options for developing GUI, but Tkinter is the fastest, easiest and most commonly used method. It is a standard Python interface to the Tk GUI toolkit shipped with Python.

**To create a tkinter app:**

- Import the module **tkinter** using the command: **import tkinter**.

- Create the main window (container)

- Add any number of [widgets](https://www.geeksforgeeks.org/python-gui-tkinter/) to the main window: Button, CheckButton, Entry, Frame, Label, Listbox, MenuButton, Menu, RadioButon, etc. Notice that in this work, I created two buttons, **Open** and **Close**, to trigger the events upon the servo. To do so, I used the method **tkinter.Button()**.

- Apply the event Trigger on the widgets.


To learn more about all the elements needed to develop GUI apps in Python, click [here](https://www.datacamp.com/community/tutorials/gui-tkinter-python?utm_source=adwords_ppc&utm_campaignid=898687156&utm_adgroupid=48947256715&utm_device=c&utm_keyword=&utm_matchtype=b&utm_network=g&utm_adpostion=&utm_creative=255798340456&utm_targetid=aud-517318242147:dsa-429603003980&utm_loc_interest_ms=&utm_loc_physical_ms=1005766&gclid=Cj0KCQjwtZH7BRDzARIsAGjbK2b7qWmKDRSlGCeQGfKBLRxoXyIyFAbHL8R-MhJntaiCZP4mxFnSrLQaAhidEALw_wcB). Moreover, the code used in this application can be found [here](https://gitlab.com/NelsonGi/nelsonproject/-/blob/master/docs/images/Images/PythonGUI.py)

```
import serial 
import tkinter


arduinoData = serial.Serial('com4',9600) // Notice that the communication in this case was declare in an static way 
                                        using the port COM4 in case another port is used then it should be changed here

// Defining two methods that will write in the serial port the commands to move the servo to different directions depending on the button pressed by the user in the GUI. 

def servoOn():
    arduinoData.write(('u').encode())
def servoOFF():
    arduinoData.write(('d').encode())

// Setting the properties of the GUI (color and geometrical dimensions)
servoControlWindow = tkinter.Tk()
servoControlWindow.title(" Servo Control")
servoControlWindow.configure(background="#94d42b")
servoControlWindow.geometry("200x150")

// Defining the btton blocks and assigning functions (turn the LED on and off) to them
btn = tkinter.Button(servoControlWindow,width=10,height=2, text="Open", command = servoOn)
btn1 = tkinter.Button(servoControlWindow,width=10,height=2, text="Close", command = servoOFF)

btn.grid(row=0,column=3)
btn1 = tkinter.Button(servoControlWindow,width=10,height=2, text="Close", command = servoOFF)

btn.grid(row=0,column=3)
btn1.grid(row=2,column=3)
servoControlWindow.mainloop()
input("Press enter to exit")
```
Notice the use of the **encode()** method, passed as an argument to the **write()** method in the **servoOn()** block. Strings in Python are Unicode by default but external hardware like Arduino's, oscilloscopes and voltmeters transmit characters as bytes. In order for a Python program to communicate with external hardware, it needs to be able to convert between Unicode strings and byte strings. This conversion is completed with the **.encode()** and **.decode()** methods. To find more information related to this aspect, click [here](https://problemsolvingwithpython.com/11-Python-and-External-Hardware/11.02-Bytes-and-Unicode-Strings/).

![](../images/Images/pythonPhoto.jpg)

## Code for GUI using Processing 
In this section, it is presented the code to develop the GUI with the programming enviroment Processing. Notice that I used the library ControlP5, which enables users to add common GUI elements to their application where mouse and keyboard based inputs are required to control application specific parameters.This code can be found [here](https://gitlab.com/NelsonGi/nelsonproject/-/blob/master/docs/images/Images/ProcessingNelson.pde).

```
ControlP5 cp5;

void setup(){
  
  size(300,400);
  printArray(Serial.list());
  String port = Serial.list()[0];
  serial_port = new Serial(this, port, 9600); // Creates an instance of the Serial port

 // To create an instance of ControlP5, call ControlP5's constructor and pass a reference of your sketch as argument 
 //("this").

  cp5 = new ControlP5(this);

 // create a button called 'Flash1' and "Flash2" and call public void medoths with the same names for executing the action. Notice that without the arguments "Flash1" or "Flash2" equal to the nam eof the methods, the buttons would appear in the main window of the GUI, but none of them would execute any action.
 cp5.addButton("Flash1")                    
   //Set the position of the button : (X,Y)
   .setPosition(100, 50)
   //Set the size of the button : (X,Y)
   .setSize(100, 80)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.PRESS);
 
  cp5.addButton("Flash2")                    // create a button called 'flash' and calls public void flash for executing the action.
   //Set the position of the button : (X,Y)
   .setPosition(100, 250)
   //Set the size of the button : (X,Y)
   .setSize(100, 80)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.PRESS);
 
  //cp5.addButton("Flash2")
  //.setPosition(100, 250)
  //.setSize(100, 80)
  //;
  
}
// Window or background specifications (color)
    void draw(){
      background(150,0,150); //(r,g,b) combination of background color.
    }
    
// These methods are called when the user click on any of the buttons. Notice that the name of the method needs to be included in the argument of the cp5.addButton() with the purpose that the button can triggered the correct methods. If this is not done, then there will be no action executed when the buttons are pressed.

  public void Flash1(){
   serial_port.write('u');
 }
 public void Flash2(){
 serial_port.write('d');
 }
```

Notice that everything happens within two main functions: **setup()** and **draw()**. The statements within the body of the setup() function are executed once when the program begins, mainly for general purpose configurations (creation of the buttons and other widgets of the GUI, appearance of these elements and the serial port used for communicating with the Arduino). Whereas all the statements withing the body of the **draw()** function are executed until the program ends; this has strong similarity to the **loop()** in Arduino. In this particular example, two methods or functions are created, Flash1() and Flash2(), that carry the information or instructions sent to the Arduino when the user presses any of the buttons. This is, under user command, two different **characters** are sent to Arduino, which depending on the value received from Processing, it moves the servo to one direction or the other.  It is very important to use the same baud rate in both Processing and Arduino (9600), specified in the command **serial_port = new Serial(this, port, 9600)**, otherwise you won't be able to make out much of the data transfered. To find valuable information related to **controlP5** library and some useful applications, click [here](http://www.sojamo.de/libraries/controlP5/).
![](../images/Images/ProcessingPhoto.jpg)

## Video

<iframe src="https://player.vimeo.com/video/401971330" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

## Second task description ("Flashing a LED with ATtiny")
In the previuos sections we could verify that in general, Processing offers nice advantages over Python in terms of visualizations in the GUI. For this reason, in this second part I decided to only use Proccessing to created a simple interface to interact with the Attiny412 through an Arduino UNO, which was used as intermediate interface because I did not have serial adapter for the conection Computer-board; notice that in this setup configuration, the Arduino is connected to the PC using the serial port. For this part, I used the board that I built in FABLAB (click [here](https://nelsongi.gitlab.io/nelsonproject/assignments/week09/) to see all the doccumentation related to this board and the building process), one Arduino UNO, one breadboard, one resitor (1k), 4 wires and the software Processing. The process of building the board, the datasheet, its structure, the programming procedure and other complementary information can be found [here](https://nelsongi.gitlab.io/nelsonproject/assignments/week09/). The PCB was programmed using the code that is shown below with an Arduino UNO as programmer, which is an alternative when the recommended programmer is not available. Notice that in this case, the Arduino is also used as a substitute of an FTDI cable (USB-to-Serial adapter). Ports 2 and 3 of the PCB were connected to ports 0 and 1 of the Arduino for transmission and reception, respectively. Moreover, the power supply provided to the PCB was obtained from VCC and GND pins of the Arduino. For the sake of simplicity and without loss of generality, I decided to use the same codes explained in previous sections as the building blocks for this new application. The only change performed to the Processing code is the name of the buttons, which are now named as "Flash1" and "Flash2" (click [here](https://gitlab.com/NelsonGi/nelsonproject/-/blob/master/docs/images/Images/ProcessingNelson.pde) to acess the code). A new code is implemented for the ATtiny in order to obtain new outcomes. This time, when the user pressses one of them, the ATtiny will flash the LED embedded in the board at a frequency of 4Hz and when the other button is pressed, the frequency changes to 10Hz.  

## Code for the ATtiny
```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3); // Create an instance "mySerial" of the serial port
        
  int pinLed = 1; // Define the pin where the LED is conected
  int cont = 0;     

  void setup() {                  
  mySerial.begin(9600);         
  pinMode(pinLed, OUTPUT);      
  pinMode(3, INPUT);  
}
void loop() {                            
if(mySerial.available()){ 
    char val = mySerial.read();  
    if(val == 'u'){      
      cont = 1;
    }
    if(val == 'd'){   
       cont = 2;                        
    }

  }
  // Depending on the value read from the serial port in the previous lines, the ATtiny will change the frequency the
  // the LED will flash
      if(cont == 1){
    
      digitalWrite(pinLed,HIGH);
      delay(250);  // Flash at 4Hz
      digitalWrite(pinLed, LOW);             
      delay(250);                          
    }
    if(cont == 2){
      
       digitalWrite(pinLed,HIGH);
       delay(100); Flash at 10 Hz
       digitalWrite(pinLed, LOW);             
       delay(100);  
    }
}

```
## Code for Arduino to function as interface between the PC and the ATtiny
```
#include <SoftwareSerial.h> 
SoftwareSerial Serial(0, 1); // RX, TX 
void setup() {              
  Serial.begin(9600);         // Initializes the port to a baud rate of 9600
  pinMode(3, INPUT);            // pin 3 as input
  } 
 void loop() {}
```
## Video

<iframe src="https://player.vimeo.com/video/456909892" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

## Conclusions
It was seen that either with Python or Processing the task of moving a servo motor interfacing with Arduino was succesfully accomplished, but 
it should be pointed out that in general Python offers less possibility in terms of GUI formats. This was the main reason why I dediced to use Processing for the second application. It was also noted that in order to send the button action 
in Python via serial communication with Arduino IDE, the strings(characters) need to first be converted (coded) which does not implies much efforts
but those are things that we need to keep in mind in order to make the whole project work as desired.

## Some other useful links

[https://pythonexamples.org/python-tkinter-set-window-size/](https://pythonexamples.org/python-tkinter-set-window-size/)

[https://github.com/sojamo/controlp5](https://github.com/sojamo/controlp5)

[https://www.youtube.com/watch?v=IJvpCbymZQE](https://www.youtube.com/watch?v=IJvpCbymZQE)

