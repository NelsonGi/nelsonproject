# 14. Networking and communications

So far, in the previous works we have accomplished different taks such as moving servos using direct serial communication between the computer
and the Arduino Board to upload the scketch and then the board executes the specific commands. The main goal this week is to stablish a serial communication between two Arduinos UNO and the ATtiny 412
in such a way that when the user press '1' (address of the Arduino) in the keyboard, the Arduino responds with the "**Arduino UNO**" printted message through the serial port. In contrast if the the user press '2' (address of the ATtiny), the ATtiny responds with the message "**ATTINY412**". More details can be obtained from the codes and the video provided in this page. The group assigment corresponding to this week can be found [here](https://henriquehilleshein.gitlab.io/olimex_pushbutton_led/assignments/week04/).

## Theoretical background and schematic diagrams

Traditionally with desktop computers, peripherals are external devices (computer mouses, keyboards, disk drives, etc.) that connect to the main computer but are independent of it.
When talking about peripherals in regards to an Arduino or other microcontrollers, the term refers to a part on the board that is dedicated to a specific task that is unrelated to the CPU.
Peripherals can be thought of as units integrated onto the Arduino boards that make specialized tasks easier. While the Arduino contains many communication peripherals such as 
UART, I2C and SPI, as shown in the figure, I will briefly look at UART which was the one used in this task.

UART stands for Universal Asynchronous Reception and Transmission and it is a simple communication protocol that allows the Arduino to communicate with serial devices
through digital pin 0 (RX), digital pin 1 (TX), and with a computer via the USB port. In asynchronous Serial Interface, the external clock signal is absent, which 
makes it rely on several parameters such as Data Flow Control, Error Control, Baud Rate Control, Transmission Control and Reception Control. 
On the transmitter side, there is a shifting of parallel data onto the serial line using its own clock.  Also it adds the start, stop and parity check bits. On the receiver side, 
the receiver extracts the data using its own clock and convert the serial data back to the parallel form after stripping off the start, stop, and parity bits.

![](../images/Images/Board.jpg)
![](../images/Images/UART.jpg)


## Code for Arduino

```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(0, 1); // RX, TX

const char node = '1'; // network address

int incomingByte;

void setup() {
  mySerial.begin(9600);
  pinMode(1, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {
     incomingByte = mySerial.read();
    if (incomingByte == node) {
      pinMode(1, OUTPUT); // open line to write
      mySerial.print("Arduino UNO \n ");  
      pinMode(1, INPUT);
      
    }
  }
}
```
## Code for ATtiny

```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3); // RX, TX

const char addr = '2'; // network address


void setup() {
  mySerial.begin(9600);
  pinMode(3, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {
    
    char incomingByte = mySerial.read();
    if (incomingByte == addr) {
      pinMode(3, OUTPUT); // open line to write
      mySerial.print("ATTINY412 \n");  
      pinMode(3, INPUT);
      delay(100);
      
    }
  }
}
```


## Video

<iframe src="https://player.vimeo.com/video/449395999" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

