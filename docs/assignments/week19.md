# Group Assigment 1 

Other students involved in this task were: [Dian Echevarria Perez](https://dechevar19.gitlab.io/gitlab-project/), [Osmel Martinez Rosabal](https://osmel-dev.gitlab.io/fablab-project/), [Henrique Hilleshein](https://henriquehilleshein.gitlab.io/olimex_pushbutton_led/).The purpose of this assigment is to compare the performance and development workflows for Harvard and Von Neumann architectures; and programming the STM32F303 nucleo board .


## Von Neumann Architecture
In a Von-Neumann architecture, the same memory and bus are used to store both data and instructions that run the program. So data transfer and instruction fetch must be scheduled.
Since you cannot access program memory and data memory simultaneously, the Von Neumann architecture is susceptible to bottlenecks and system performance is affected.

It consists of three distinct components: a central processing unit (CPU), memory unit, and input/output (I/O) interfaces. The CPU is the heart of the computer system that consists of three main components: 
the Arithmetic and Logic Unit (ALU), the control unit (CU), and registers. The ALU is responsible for carrying out all arithmetic and logic operations on data, whereas the control unit determines the order of flow of 
instructions that need to be executed in programs by issuing control signals to the hardware. The registers are basically temporary storage locations that store addresses of the instructions that need to be executed.
The memory unit consist of RAM, which is the main memory used to store program data and instructions. The I/O interfaces allows the users to communicate with the outside world such as storage devices.
In general processors needs two clock cicles to complete an instruction. Pipelining the instruction is not possible with this architecture
![](../images/Images/Vonneumann.jpeg)

## Harvard Architecture
It is a computer architecture with physically separate storage and signal pathways for program data and instructions. 
Unlike Von Neumann architecture which employs a single bus to both fetch instructions from memory and transfer data from one part of a computer to another,
Harvard architecture has separate memory space for data and instruction. Both the concepts are similar except the way they access memories. The idea behind 
the Harvard architecture is to split the memory into two parts – one for data and another for programs. The terms was based on the original Harvard Mark I relay 
based computer which employed a system that would allow both data and transfers and instruction fetches to be performed at the same time.
Real world computer designs are actually based on modified Harvard architecture and are commonly used in microcontrollers and DSP (Digital Signal Processing).
![](../images/Images/Harvard.jpeg)

## Programming the STM32F303 nucleo board
Even though a few platforms exist to program the STM32 family board (STM32Cube, Arduino IDE, KEIL, etc), in this work, we exploit the advantages of the Mbed compiler. This is a web app that provides a lightweight online C/C++ IDE that is pre-configured to let the user quickly write programs, compile and download them to run on the mbed Microcontroller. This online platform is available for Windows, Mac, iOS, Android and Linux.

## General aspects before programming the board
Since we use the internal STLink programmer debugger, the jumpers in CN2 should be present; in contrast, if we wish to use the SWD, the jumpers should be removed and use the external programmer. Notice that in this project, we set the JP5 to U5V in order to power the board from USB; but if we want to use an external power source, then JP5 should be shifted to E5V. Keep in mind also that if we use 3.3v as external source, the ST_LINK debugger will not work. 
![](../images/Images/STLink_jumper.jpg) 

In case the board is connected to the PC before installing the required drivers, the PC device manager may report some Nucleo interfaces as “Unknown”. Thus, the proper driver for ST-LINK/V2-1 must be installed. It can be downloaded from: https://os.mbed.com/platforms/ST-Nucleo-F303RE/. Scroll down until you see the following information that links to the place where the drivers can be downloaded.
![](../images/Images/Stlink_upgrade.jpg)
Steps to program the board

1.  Open the link http://www.mbed.com/

2.  Click on Mbed OS as shown given in below image.
![](../images/Images/Step1.jpg)

3.  If this is your first time using Mbed, then click on Log in/Sign up to create your account.
4.  Click on **Compiler** to open online compiler. 
![](../images/Images/Step2.jpg)
5.  Click on **No device selected** and select “NUCLEO-F303RE”.
![](../images/Images/Step3.jpg)
6. Now right click on **My Programs** and click on **New Program**.
![](../images/Images/Step4.jpg)

7.A new window will open and in the template option, we can select predefined "sketchs" for different simple applications;in this case I selected the corresponding to LED blinking and made some trasformations that included the USER_BUTTON, which determines whether the LED should blink or not. 
![](../images/Images/step5.jpg)


8. Click on main.cpp will open the mbed led blinking program and then click on **Compile** button.
![](../images/Images/Compiling.jpg)
After compiling, the **.bin** will be automatically downloaded. By clicking on **Build Details** (the orange line) the application displays the memory usage. This shows the size of program code and any constant (const) variables that will end up in FLASH, and the size of data variables that end up in main RAM.
![](../images/Images/Compiling2.jpg) 
9.   Open the mbed drive and paste **.bin** file (downloaded from web compiler). 

## Code Description
The purpose of the following code is to blink the LED2 when the USER_BUTTON is pressed (both marked in red in the picture). When the botton in not held, the LED2 should be off. 
![](../images/Images/STM32_board.jpg)

```
#include "mbed.h"
#include "platform/mbed_thread.h"

int main()
{
    // Initialise the digital pin LED1 as an output
    DigitalOut led(LED1);
    DigitalIn mybutton(USER_BUTTON);
   while(1) {
 
        if(mybutton == 0) {
            led = 1; // LED is ON
            wait(0.6);
            led = 0;
            wait(0.6);
        } else {
            led = 0; // LED is OFF
        }
    }
}

```
## Video "Blinking a LED with the NUCLEO-F303RE"
<iframe src="https://player.vimeo.com/video/443290039" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

## Comparision between ATmega328 and STM32F303
Notice that in the folllowing table appears the STM32F103, but from the datasheet that can be found in one of the links
provided bellow it can be seen the equivalence with the STM32F303 (this is the one we used in the project).
![](../images/Images/Comparision.jpg)

## Useful link

(https://www.youtube.com/watch?v=weag4UxassY)

(https://os.mbed.com/platforms/ST-Nucleo-F303RE/)

(https://www.st.com/resource/en/user_manual/dm00105823-stm32-nucleo-64-boards-mb1136-stmicroelectronics.pdf)

