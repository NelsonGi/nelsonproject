import serial 
import tkinter


arduinoData = serial.Serial('com4',9600)
def servoOn():
    arduinoData.write(('u').encode())
def servoOFF():
    arduinoData.write(('d').encode())

servoControlWindow = tkinter.Tk()
servoControlWindow.title(" Servo Control")
servoControlWindow.configure(background="#94d42b")
servoControlWindow.geometry("250x150")

btn = tkinter.Button(servoControlWindow,width=10,height=2, text="Open", command = servoOn)
btn1 = tkinter.Button(servoControlWindow,width=10,height=2, text="Close", command = servoOFF)

btn.grid(row=0,column=3)
btn1.grid(row=2,column=3)
servoControlWindow.mainloop()
input("Press enter to exit")
