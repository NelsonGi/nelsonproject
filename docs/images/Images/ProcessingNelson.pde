import controlP5.*; 
import processing.serial.*;

Serial serial_port;

ControlP5 cp5;

void setup(){
  
  size(300,400);
  printArray(Serial.list());
  String port = Serial.list()[0];
  serial_port = new Serial(this, port, 9600); 


  cp5 = new ControlP5(this);

 // cp5.addButton("Flash1")
 // .setPosition(100, 50)
  //.setSize(100, 80)
 // ;
 
 cp5.addButton("Flash1")                    // create a button called 'flash' and calls public void flash for executing the action.
   //Set the position of the button : (X,Y)
   .setPosition(100, 50)
   //Set the size of the button : (X,Y)
   .setSize(100, 80)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.PRESS);
 
  cp5.addButton("Flash2")                    // create a button called 'flash' and calls public void flash for executing the action.
   //Set the position of the button : (X,Y)
   .setPosition(100, 250)
   //Set the size of the button : (X,Y)
   .setSize(100, 80)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.PRESS);
 
  //cp5.addButton("Flash2")
  //.setPosition(100, 250)
  //.setSize(100, 80)
  //;
  
}
    void draw(){
      background(150,0,150);
    }
    

 void Flash1(){
   serial_port.write('u');
 }
 void Flash2(){
 serial_port.write('d');
 }
