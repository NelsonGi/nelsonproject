#include <Servo.h>    
Servo servoMotor;     
int servPin = 2;      
int pulseMin = 1000;  
int pulseMax = 2000;  

void setup() {       
 servoMotor.attach(servPin,pulseMin,pulseMax); 
 Serial.begin(9600);                      
 }

void loop() {      

  if(Serial.available()){
    
    char val = Serial.read();  
    if(val == 'u'){            
                               
       servoMotor.write(180);   
       delay(200); 
    }
    if(val == 'd'){
       servoMotor.write(0);  
    }
  }
}
